import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  display : boolean = false;
  headerText : string;
  bodyText : string;
  passwordChange = new FormGroup({
    Password: new FormControl(''),
    PasswordC: new FormControl('')
  });
  constructor(private httpClient : HttpClient) { }

  ngOnInit(): void {

  }

  public async changePassword(){
    if(this.passwordChange.get("Password").value == this.passwordChange.get("PasswordC").value){
      var sendObj = {
        username: localStorage.getItem("username"),
        password: this.passwordChange.get("Password").value
      }
      console.log(sendObj);
    }
    //   var returnVal = await this.httpClient.changePassword(sendObj).catch(error =>{
    //     this.bodyText = "Server error";
    //     this.headerText = "Error!";
    //     this.display = true;
    //     this.passwordChange.reset("");
    //   });
    //   if(returnVal.success){
    //     this.bodyText = "Passwort erfolgreich geändert";
    //     this.headerText = "Erfolgreich!";
    //     this.display = true;
    //     this.passwordChange.reset("");
    //   }else{
    //     this.bodyText = "Fehler beim ändern des Passwort";
    //     this.headerText = "Fehler!";
    //     this.display = true;
    //     this.passwordChange.reset("");
    //   }
    // }else{
    //   this.bodyText = "Passwörter müssen übereinstimmen";
    //   this.headerText = "Fehler!";
    //   this.display = true;
    //   this.passwordChange.reset("");
    // }
  }

  public closeAlert():void{
    this.display = false;
  }
}
