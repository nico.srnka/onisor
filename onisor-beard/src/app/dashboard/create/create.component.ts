import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  display: boolean = false;
  errorDate: string;
  idForEdit: string;
  displaySuccess: boolean = false;
  entry = new FormGroup({
    Date: new FormControl(''),
    Length: new FormControl(''),
    TimeSpan: new FormControl(''),
    Brand: new FormControl(''),
    Tool: new FormControl(''),
    ShaveType: new FormControl(''),
    Comment: new FormControl(''),
  });

  constructor(private httpClient: HttpClient, private router: Router) {}

  ngOnInit(): void {}

  public async createEntry() {
    let date = new Date(this.entry.get('Date').value).toLocaleDateString();
    let sendObj = this.entry.value;
    sendObj.Date = date;
    sendObj.username = localStorage.getItem('username');
    console.log(sendObj);
    let response = await this.httpClient
      .put('http://localhost:3000/api/addDiary', sendObj)
      .toPromise();
    if (response) {
      this.displaySuccess = true;
    }
  }

  public showDialog(): void {
    this.display = true;
  }

  public showSuccessDialog(): void {
    this.displaySuccess = true;
  }
  public closeAlertSuccess(): void {
    this.displaySuccess = false;
  }
  public closeAlert(): void {
    this.display = false;
  }
  public goToEntry(): void {
    this.router.navigate(['dashboard/edit/' + this.idForEdit]);
  }
}
