export class BeardEntry{
    Date : string;
    Length : number;
    Comment : string;
    TimeSpan : number;
    Brand : string;
    Id : string;
    Tool : string;
    ShaveType : string;
}