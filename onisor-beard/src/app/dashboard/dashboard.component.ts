import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AuthGuard } from '../auth/auth.guard';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  items: MenuItem[];
  activeItem: MenuItem;
  constructor(private authGuard: AuthGuard, private router: Router) {}

  ngOnInit(): void {
    this.items = [
      {
        label: 'Overview',
        routerLink: ['overview'],
      },
      {
        label: 'Add Entry',
        routerLink: ['create'],
      },
      {
        label: 'Account',
        routerLink: ['account'],
      },
      {
        label: 'Logout',
        command: (event) => {
          localStorage.setItem('isLoggedIn', 'false');
          localStorage.setItem('username', '');
          this.router.navigate(['login']);
        },
      },
    ];
    this.activeItem = this.items[0];
  }
}
