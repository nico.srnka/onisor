import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { CreateComponent } from './create/create.component';

import { DashboardComponent } from './dashboard.component';
import { EditComponent } from './edit/edit.component';
import { OverviewComponent } from './overview/overview.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: 'create', component: CreateComponent },
      { path: 'edit', component: EditComponent },
      { path: 'overview', component: OverviewComponent },
      { path: 'account', component: AccountComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
