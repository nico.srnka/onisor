import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {BeardEntry} from '../beardEntry';
@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  beardEntries : Array<BeardEntry>;
  constructor(private router : Router, private httpClient : HttpClient) { }

  ngOnInit(): void {
    this.beardEntries = new Array<BeardEntry>();

    this.loadEntries();
  }

  public async loadEntries(){
    this.beardEntries = await this.getEntries(localStorage.getItem("username"));
    
  }

  public async getEntries(name : string): Promise<Array<BeardEntry>> {
    return this.httpClient.get<Array<BeardEntry>>("http://localhost:3000/api/getDiarys/"+ name).toPromise();
  }
}
