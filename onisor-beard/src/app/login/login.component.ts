import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    user: new FormControl(''),
    pwd: new FormControl(''),
  });

  registerForm = new FormGroup({
    user: new FormControl(''),
    pwd: new FormControl(''),
  });

  loginInput : boolean = true;
  loginErrorName : string;
  registerErrorName : string;
  constructor(private httpClient : HttpClient, private router: Router,private authService: AuthGuard,) { }

  ngOnInit(): void {
  }


  public async checkLogin(){
    console.log(this.loginForm.value);
    var checkLogin = await this.httpClient.post('http://localhost:3000/api/login', {username:"oni", password: "tmp"}).toPromise();
    console.log(checkLogin);
     if(checkLogin){
    alert("login success");
    localStorage.setItem('isLoggedIn', 'true');
      localStorage.setItem('username', "oni");
      this.router.navigate(['/dashboard/overview']);
      }
  }

  public async registerSubmit(){
    console.log(this.loginForm.value);
    var checkLogin = await this.httpClient.put('http://localhost:3000/api/register', {username:"oni", password: "tmp"}).toPromise();
    console.log(checkLogin);
     if(checkLogin){

      }
  }

  public switch(){
    if(this.loginInput){
      this.loginInput = false;
    }else{
      this.loginInput = true;
    }
  }
}
