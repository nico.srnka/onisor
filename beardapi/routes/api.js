var express = require('express');
var router = express.Router();
const bodyParser = require("body-parser");
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const crypto = require('crypto');
// rethinkdb
const r = require('rethinkdb');
var databaseName = process.env.RDB_DATABASE;
var tableName = "User"; // set table name
var tableName2 = "Diary"; // set table name
var connection = null;
r.connect( {host: 'localhost', port: 28015, db: 'bearddb'}, function(err, conn) {
    if (err) throw err;
    connection = conn;
})

/* check login */
router.post('/login/', (request,response ) => {
    let userToCheck = request.body;
    console.log(crypto.createHash('md5').update(userToCheck.password).digest('hex'))
    r.table(tableName).filter({username: userToCheck.username, password : crypto.createHash('md5').update(userToCheck.password).digest('hex')}).count().run(connection).then(cursor=>{
        if(cursor == 1){
            let data = {
                'success': true
            };
            response.json(data);
        }else{
            let data = {
                'success': false
            };
            response.json(data);
        }
        
    });
});

router.put('/register/', (request,response ) => {
    let userToCheck = request.body;
    userToCheck.password = crypto.createHash('md5').update(userToCheck.password).digest('hex');
    console.log(userToCheck);
    r.table(tableName).filter({username: userToCheck.username}).count().run(connection).then(cursor=>{
        if(cursor == 0){
            r.table(tableName).insert(userToCheck).run(connection, function(err, result) {
                if (err) throw err;
                let data = {
                    'success': true,
                    'message': "User added.",
                };
                response.json(data);
            });
        }else{
            let data = {
                'success': false,
                'message': "Username already taken.",
            };
            response.json(data);
        }
    });   
});

router.get('/getDiarys/:username', (request,response ) => {
    let userToCheck = request.params.username;
    console.log(userToCheck);

    r.table(tableName2).filter({username : userToCheck})
        .run(connection)
        .then(cursor => cursor.toArray())
        .then(result => {
            response.json(result);
        })
        .catch( error => console.log(error));
});

router.put('/addDiary/', (request,response ) => {
    let userToCheck = request.body;
    //username, date, ois
    console.log(userToCheck);
    r.table(tableName2).filter({username: userToCheck.username, Date : userToCheck.Date}).count().run(connection).then(cursor=>{
        if(cursor == 0){
            r.table(tableName2).insert(userToCheck).run(connection, function(err, result) {
                if (err) throw err;
                let data = {
                    'success': true,
                    'message': "Diary entry added.",
                };
                response.json(data);
            });
        }else{
            r.table(tableName2).filter({username : userToCheck.username, Date : userToCheck.Date}).run(connection).then(cursor => cursor.toArray())
            .then(result => {
                let data = {
                    'success': false,
                    'id' : result[0].id,
                    'message': "Date already taken. ",
                    
                };
                response.json(data);
            });
        }
    });   
});

router.get('/getDiary/:id', (request,response ) => {
    let userToCheck = request.params.id;
    console.log(userToCheck);    
     r.table(tableName2)
            .get(userToCheck)
            .run(connection)
            .then(result => {
                // logic if you want to set
                response.json(result);
            })
            .catch( error => console.log(error));
});

router.get('/getChartData/:username/:startDate/:endDate', (request,response ) => {
    let username = request.params.username;
    let startDate = request.params.startDate;
    let endDate = request.params.endDate;
    var returnVal = new Array();
     r.table(tableName2)
            .filter({username: username})
            .run(connection, function(err, cursor){
                if(err){
                   throw err;
                }
                cursor.each(function(err,row) {
                   let dataRow = row;
                    if(new Date(dataRow.Date).getTime() >= new Date(startDate).getTime() && new Date(dataRow.Date).getTime() <= new Date(endDate).getTime()){
                        returnVal.push(dataRow);
                    }
                }, function(){
                    console.log(returnVal);
                    response.json(returnVal);
                })
                
            } 
)
});

router.put('/editDiary/', (request,response ) => {
    let userToCheck = request.body;
    //username, date, ois
    console.log(userToCheck);
    r.table(tableName2)
      .get( userToCheck.id )
      .update( {
        'AirTemp': userToCheck.AirTemp,
        'ChlorValue': userToCheck.ChlorValue,
        'Comment' : userToCheck.Comment,
        'PHValue' : userToCheck.PHValue,
        'WaterTemp': userToCheck.WaterTemp,
        'FilterRewindMin': userToCheck.FilterRewindMin
      })
      .run( connection )
      .then(result => {
        let data = {
            'success': true,
            'message': "Changed. ",
            
        };
          response.send(data);
      })
      .catch(error => console.log(error));
    });

router.put('/changePassword/', (request,response ) => {
    let userToCheck = request.body;
    //username, date, ois
    userToCheck.password = crypto.createHash('md5').update(userToCheck.password).digest('hex');
    r.table(tableName)
    .filter({username: userToCheck.username})
    .update( {
    'password': userToCheck.password
    })
    .run( connection )
        .then(result => {
            let data = {
                'success': true,
                'message': "Changed. ",
            };
              response.send(data);
          })
          .catch(error => console.log(error));
        });

router.delete( '/deleteDiary/:id', (request,response ) => {
    let userToCheck = request.params.id;
    console.log(userToCheck);
    r.table(tableName2)
        .get(userToCheck)
        .delete()
        .run(connection)
        .then(result => {
            let data = {
                'success': true,
                'message': "DiaryEntry successfully deleted",
            };
            response.json(data);
        })
        .catch(error => console.log(error));
  });


module.exports = router;